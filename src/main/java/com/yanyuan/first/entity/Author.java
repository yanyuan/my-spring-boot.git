package com.yanyuan.first.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: yanyuan
 * @Date: 2020/9/29 11:50
 * @Description:
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Author {
    private String name;
    private String avatar;
}
